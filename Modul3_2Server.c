#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/msg.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>

int main()
    {
    int msqid;
    char pathname[] = "/home/p4ch1m4n/testdir/testfile";
    key_t key; /* IPC ключ */
    int i, len, maxlen; /* Cчетчик цикла и длина информативной части сообщения */
   char strpid[20];
    struct Msg
	{
	    long mtype;
	    long clientpid;
	} msgbuf;
    struct ServMsg
	{
	    long mtype;
	    char msg[20];
	} sndbuf;
    if ((key = ftok(pathname, 0)) < 0)
	{
	printf("Can't generate key\n");
	exit(-1);
	}

    if ((msqid = msgget(key, 0666 | IPC_CREAT)) < 0)
	{
	printf("Can't get msqid\n");
	exit(-1);
	}
    for (;;)
	{
	maxlen = 20;
	if ((len = msgrcv(msqid, (struct Msg*) &msgbuf, maxlen, 0, 0)) < 0)
	    {
	    printf("Can't receive message from queue\n");
	    exit(-1);
	    }
	if (msgbuf.mtype == 1)
	    {
	    sndbuf.mtype = 2;
	    sprintf(strpid,"%ld",msgbuf.clientpid);
	    strcpy(sndbuf.msg, "Your PID= ");
        strcpy(sndbuf.msg,strpid);
	    len = strlen(sndbuf.msg) + 1;
	    if (msgsnd(msqid, (struct ServMsg*) &sndbuf, len, 0) < 0)
		{
		printf("Can't send message to queue\n");
		msgctl(msqid, IPC_RMID, (struct msqid_ds*) NULL);
		exit(-1);
		}
        printf("done\n");
	    }
	}

    return 0;
    }

