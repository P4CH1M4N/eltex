#include <sys/types.h>
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <stddef.h>
#include <signal.h>
int pblock,cblock;
void my_handler1(int nsig){
    printf("sigusr2 recieved\n");
pblock=0;
}
void my_handler2(int nsig){
    printf("sigusr1 recieved\n");
cblock=0;
}

int main()
    {
    int fd[2], result;
    int size;
    char resstring[14];
    pid_t parent;
    parent=getpid();
    /* Попытаемся создать pipe */
    if (pipe(fd) < 0)
	{
	/* Если создать pipe не удалось, печатаем об этом сообщение и прекращаем работу */
	printf("Can't create pipe\n");
	exit(-1);
	}
    /* Порождаем новый процесс */
    result = fork();
    if (result < 0)
	{
	/* Если создать процесс не удалось, сообщаем об этом и завершаем работу */
	printf("Can't fork childP\n");
	exit(-1);
	}
    else if (result == 0)  /* CHILD*/
		{
	cblock=1;
	(void)signal(10, my_handler2);
	while(cblock!=0)
	    {

	    }
		size = read(fd[0], resstring, 14);
		if (size < 0)
		    {
		    printf("Can't read stringC\n");
		    exit(-1);
		    }
		printf("%s\n", resstring);
		size = write(fd[1], "ChildsMessage", 13);
		if (size != 13)
			    {
			    /* Если записалось меньшее количество байт, сообщаем об ошибке и завершаем работу */
			    printf("Can't write all stringC\n");
			    exit(-1);
			    }

		printf("Child exit\n");
		close(fd[0]);
		close(fd[1]);
		sleep(2);
		kill(parent,12);
		printf("siguser2 sent\n");
		}
    else //PARENT
	{
	pblock=1;
	size = write(fd[1], "ParentMessage", 14);
	if (size != 14)
	    {
	    /* Если записалось меньшее количество байт, сообщаем об ошибке и завершаем работу */
	    printf("Can't write all stringP\n");
	    exit(-1);
	    }
	(void)signal(12, my_handler1);
	sleep(2);
	kill(result,10);
	while(pblock!=0)
	    {

	    }
	size = read(fd[0], resstring, 13);
	    	if (size < 0)
	    	    {
	    	    /* Если прочитать не смогли, сообщаем об ошибке и завершаем работу */
	    	    printf("Can't read stringP\n");
	    	    exit(-1);
	    	    }
	    	printf("%s\n", resstring);
	    	close(fd[0]);
	    	close (fd[1]);
	printf("Parent exit\n");
	}

    return 0;
    }
