#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/msg.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

int main()
    {
    int msqid;
    char pathname[] = "/home/p4ch1m4n/testdir/testfile";
    key_t key; /* IPC ключ */
    int i, len, maxlen; /* Cчетчик цикла и длина информативной части сообщения */
   char strpid[20];
    struct Msg
	{
	    long mtype;
	    long clientpid;
	} msgbuf;
    struct ServMsg
	{
	    long mtype;
	    char msg[20];
	} rcvbuf;
    if ((key = ftok(pathname, 0)) < 0)
	{
	printf("Can't generate key\n");
	exit(-1);
	}

    if ((msqid = msgget(key, 0666 | IPC_CREAT)) < 0)
	{
	printf("Can't get msqid\n");
	exit(-1);
	}
    msgbuf.clientpid=getpid();
    msgbuf.mtype=1;

    if (msgsnd(msqid, (struct Msg*) &msgbuf, sizeof(msgbuf), 0) < 0)
   		{
   		printf("Can't send message to queue\n");
   		msgctl(msqid, IPC_RMID, (struct msqid_ds*) NULL);
   		exit(-1);
   		}
	maxlen = 20;
	if ((len = msgrcv(msqid, (struct ServMsg*) &rcvbuf, maxlen, 0, 0)) < 0)
	    {
	    printf("Can't receive message from queue\n");
	    exit(-1);
	    }
	if (rcvbuf.mtype == 2)
	    {
		printf("%s\n",rcvbuf.msg);
	    }

    return 0;
    }
